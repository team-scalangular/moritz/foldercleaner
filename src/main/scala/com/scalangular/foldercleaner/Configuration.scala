package com.scalangular.foldercleaner

import cats.Show
import cats.implicits._

import java.nio.file.{InvalidPathException, Path, Paths}
import java.time.Duration
import scala.util.matching.Regex

case class Configuration(automatic: Boolean, folder: Path, maxLastModified: Option[Duration], toDelete: Option[Regex],
                         verbose: Boolean, maxSize: Option[Long])

object Configuration {
  implicit val showOptions = new Show[Configuration] {
    override def show(t: Configuration): String = {
      s"path: ${t.folder.toAbsolutePath}"
    }
  }
}


trait OptionsReader {
  def read(args: List[String]): Either[String, Configuration]
}

object OptionsReaderInstance {
  implicit val optionsReader: OptionsReader = new OptionsReader {
    override def read(args: List[String]): Either[String, Configuration] = {
      def getPath = try {
        val rawPath = args(args.indexOf("-p") + 1)
        Right(Paths.get(rawPath))
      } catch {
        case _: IndexOutOfBoundsException => Left("No path provided")
        case e: InvalidPathException => {
          e.printStackTrace()
          Left("Not a valid path")
        }
      }

      def getTime = try {
        if (!args.contains("-t")) Right(none[Duration])
        else {
          val rawTime = args(args.indexOf("-t") + 1)
          Right(Duration.ofDays(rawTime.toLong).some)
        }
      } catch {
        case _: IndexOutOfBoundsException => Right(none[Duration])
      }

      def getMaxSize = {
        if (!args.contains("-s")) Right(none[Long])
        else {
          val rawSize = args(args.indexOf("-s") + 1)
          Right(rawSize.toLongOption)
        }
      }

      for {
        path <- getPath
        maxDuration <- getTime
        maxSize <- getMaxSize
      } yield Configuration(automatic = false, folder = path, maxLastModified = maxDuration, toDelete = none[Regex],
        verbose = false, maxSize = maxSize)
    }
  }
}