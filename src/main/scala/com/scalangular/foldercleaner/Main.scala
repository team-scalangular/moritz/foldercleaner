package com.scalangular.foldercleaner

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.scalangular.foldercleaner.CleanerInstance.cleanerInstance.clean
import com.scalangular.foldercleaner.OptionsReaderInstance.optionsReader.read

object Main extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    read(args) match {
      case Left(message) => IO {
        println(s"Couldn't execute folder cleaning\n$message")
      }.as(ExitCode.Error)
      case Right(value) =>
        for {
          _ <- IO {
            println(s"Options: ${value.show}")
          }
          result <- clean(value).handleErrorWith(e => IO {
            println(s"Couldn't execute cleanup: ${e.getMessage}")
            e.printStackTrace()
            ExitCode.Error
          })
        } yield result
    }
  }
}


