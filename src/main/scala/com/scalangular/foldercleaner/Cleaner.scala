package com.scalangular.foldercleaner

import cats.effect.{ExitCode, IO}

import java.io.FileNotFoundException
import java.nio.file.{Files, Path}
import java.time.{Duration, Instant}
import scala.jdk.StreamConverters._

trait Cleaner {
  def clean(options: Configuration): IO[ExitCode]
}

object CleanerInstance {

  implicit val cleanerInstance: Cleaner = (options: Configuration) => {
    existsAndIsFolder(options.folder) *>
      getFiles(options.folder)
        .flatMap(filesToFilter => filterFiles(filesToFilter, options))
        .flatMap(filesToDelete => deleteFiles(filesToDelete))
        .as(ExitCode.Success)
  }

  private def existsAndIsFolder(path: Path): IO[Unit] = {
    val valid = Files.exists(path) && Files.isDirectory(path)
    if (!valid) {
      IO.raiseError(new FileNotFoundException(s"Folder $path doesnt exits or is a file"))
    } else {
      IO(println(s"Folder $path is valid"))
    }
  }

  private def deleteFiles(paths: List[Path]): IO[Unit] = {
    IO {
      println("Deleting: " + paths.mkString(",\n"))
      val size = paths.foldLeft(0L)((acc, cur) => {
        acc + Files.size(cur)
      })
      paths.foreach(Files.delete)
      println(s"Freed ${size / 1e+6}mb")
    }
  }

  private def getFiles(base: Path): IO[List[Path]] = {
    IO(Files.walk(base).toScala(List))
  }

  private def isDirectory(path: Path): Boolean = Files.isDirectory(path)

  private def isFile(path: Path): Boolean = !isDirectory(path)

  private def isToOld(path: Path, maxAge: Duration, currentTime: Instant): Boolean = {
    val lastModified = Files.getLastModifiedTime(path)
    maxAge.compareTo(Duration.between(lastModified.toInstant, currentTime)) <= 0
  }

  private def isToBig(path: Path, maxSize: Long): Boolean = {
    (Files.size(path) / 1e+6) > maxSize
  }

  private def filterFiles(paths: List[Path], options: Configuration): IO[List[Path]] = {
    for {
      currentTime <- IO(Instant.now())
      onlyFiles <- IO(paths.filter(isFile))
      ageChecked <- IO(onlyFiles.filter(isToOld(_, options.maxLastModified.getOrElse(Duration.ZERO), currentTime)))
      sizeChecked <- IO(ageChecked.filter(isToBig(_, options.maxSize.getOrElse(-1))))
    } yield sizeChecked
  }

}
