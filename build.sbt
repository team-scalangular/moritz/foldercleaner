import sbt._
import scala.sys.process._
import java.io.File

val appName = "foldercleaner"
name := "foldercleaner"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.13.1"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.3.0"
libraryDependencies += "org.typelevel" %% "cats-effect" % "2.3.0"

libraryDependencies += "co.fs2" %% "fs2-core" % "2.5.0" // For cats 2 and cats-effect 2

// optional I/O library
libraryDependencies += "co.fs2" %% "fs2-io" % "2.5.0"
// scalac options come from the sbt-tpolecat plugin so need to set any here

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)

lazy val nativeImage =
  taskKey[File]("Build a standalone executable using GraalVM Native Image")

nativeImage := {
  import sbt.Keys.streams
  val assemblyFatJar = assembly.value
  val assemblyFatJarPath = assemblyFatJar.getParent
  val assemblyFatJarName = assemblyFatJar.getName
  val outputPath = (baseDirectory.value / "out").getAbsolutePath
  val outputName = s"$appName.exe"
  val nativeImageDocker = s"linux/graalvm-native-image"

  val cmd = s"""docker run
               | --volume ${assemblyFatJarPath}:/opt/assembly
               | --volume ${outputPath}:/opt/native-image
               | ${nativeImageDocker}
               | --static
               | -H:+ReportExceptionStackTraces --no-fallback
               | --allow-incomplete-classpath
               | -jar /opt/assembly/${assemblyFatJarName}
               | ${outputName}""".stripMargin.filter(_ != '\n')
//--report-unsupported-elements-at-runtime dunno xD
  val log = streams.value.log
  log.info(s"Building native image from ${assemblyFatJarName}")
  log.debug(cmd)
  val result = (cmd.!(log))

  if (result == 0) file(s"${outputPath}/${outputName}")
  else {
    log.error(s"Native image command failed:\n ${cmd}")
    throw new Exception("Native image command failed")
  }
}